/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2021 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Air intake of rear stem of german DMU class 294
 * Lufteinlass Front hinterer Vorbau Baureihe 294
 *
 * Ressources/Quellen
  * [1]: Measures taken from pictures
  * [2]: Measures taken from model
 */

faktor = 1 / 87; // Maßstab


// Maße in mm

// allgemeine Maße

// Lüfter
luefter_b = 3.7; // Breite
luefter_h = 4.3; // Höhe
luefter_t = 1.27; // Tiefe
luefter_rahmen_b = 0.3; // Rahmenbreite
luefter_rippe_b = 0.52; // Rippenbreite

// Montage
$fn = $preview == true ? 64 : 128;

rahmen();
for (i = [-5 : 2 : 5]) {
    translate([i * luefter_rippe_b / 2, 0, 0]) rippe();
}

// Rahmen
module rahmen() {
    rahmen_b = luefter_rahmen_b; // Breite
    rahmen_t = luefter_t; // Tiefe

    translate([0, rahmen_t / 2, 0]) rotate([90, 0, 0]) difference() {
        cube(size = [luefter_b, luefter_h, rahmen_t], center = true);
        cube(size = [luefter_b - 2 * rahmen_b, luefter_h - 2 * rahmen_b, rahmen_t + 1], center = true);
    }
}

// Rippe
module rippe() {
    rippe_h = luefter_h - luefter_rahmen_b * 2; // Höhe
    rippe_b = luefter_rippe_b; // Breite
    rippe_r = 0.05; // Radius an der Spitze
    rippe_t = 0.4; // Tiefe

    hull() {
        translate([0, rippe_r + 0.1, 0]) cylinder(r = rippe_r, h = rippe_h, center = true);
        translate([0, luefter_t - (luefter_t - rippe_t) / 2, 0]) cube(size = [rippe_b, luefter_t - rippe_t, rippe_h], center = true);
    }
}